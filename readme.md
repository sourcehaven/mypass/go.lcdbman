# Local DB Manager

This subproject is to help you with managing the local database.
This is a package, and will not be made into a separate service;
however other services could depend on this package.

## Example usage

```go
db := ... // get your db connection
pwDao := NewSimpleDao[uint, model.Password](Config{DB: db}) // dao for managing passwords
fdDao := NewSimpleDao[uint, model.Folder](Config{DB: db})   // dao for managing folders
```

This dao can help you with managing child and/or parent elements.
To enable this feature, you need to use the `K4L` mode defined by the interface,
and also, your models should follow the specification below.

```go
type dummyGormMP struct {
	gorm.Model
	SomeString string
	ChiID      uint `gormx:"childkey"`
	ParID      uint `gormx:"parentkey"`
}
```

To enable this feature, you should be using the above specified `gormx`
tag to specify a `childkey`, and a `parentkey` if you need one.

Your models should also embed the gorm.Model type to work with this dao implementation.
