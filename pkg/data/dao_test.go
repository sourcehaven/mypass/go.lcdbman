package data

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/internal/data"
	"gorm.io/gorm"
	"reflect"
	"testing"
)

type daoRes struct {
	db *gorm.DB
}

type noGormModelV1 struct {
	SomeString string
}

type noGormModelV2 struct {
	Model      any
	SomeString string
}

type modelV42 struct {
	gorm.Model
	SomeString string
	SomeInt    int
}

type modelV43 struct {
	gorm.Model
	SomeString string
	SomeInt    int
	ChildID    int `gormx:"childkey"`
	ParentID   int `gormx:"parentkey"`
}

func setupDao(t *testing.T) *daoRes {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))

	testPws := []modelV42{
		{SomeString: "jaksd wjiaudais aoiNAS", SomeInt: 587},
		{SomeString: "DAJ KdjaKjkda", SomeInt: 98},
		{SomeString: "DMSKAL dwka", SomeInt: 292},
		{SomeString: "jaais aoAS", SomeInt: 52},
	}
	for _, password := range testPws {
		assert.NoError(t, db.Create(&password).Error)
	}
	return &daoRes{db}
}

func teardownDao(t *testing.T, r *daoRes) {
	assert.NoError(t, r.db.Exec(fmt.Sprintf("DROP TABLE IF EXISTS %ss;", reflect.TypeOf(modelV42{}).Name())).Error)
}

func TestNewSimpleDao(t *testing.T) {
	assert.Panics(t, func() { NewSimpleDao[uint, noGormModelV1](Config{}) })
	assert.Panics(t, func() { NewSimpleDao[uint, noGormModelV2](Config{}) })
}

func TestSimpleDao_Create(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	var c int64
	db.Model(&modelV42{}).Count(&c)
	assert.Equal(t, int64(0), c)

	var m []modelV42
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	assert.NoError(t, dao.Create(&modelV42{SomeString: "my djauwd", SomeInt: 42}))
	assert.NoError(t, dao.Create(&modelV42{SomeString: "dkao woas", SomeInt: 3}))
	assert.NoError(t, dao.Create(&modelV42{SomeString: "idoa nwia", SomeInt: 6}))
	assert.NoError(t, db.Find(&m).Error)
	assert.Len(t, m, 3)
}

func TestSimpleDao_CreateAll(t *testing.T) {
	db := data.Connect("file::memory:")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	var c int64
	db.Model(&modelV42{}).Count(&c)
	assert.Equal(t, int64(0), c)

	m := []*modelV42{
		{SomeString: "my djauwd", SomeInt: 42},
		{SomeString: "dkao woas", SomeInt: 3},
		{SomeString: "idoa nwia", SomeInt: 6},
	}
	var lst []modelV42
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	assert.NoError(t, dao.CreateAll(m))
	assert.NoError(t, db.Find(&lst).Error)
	assert.Len(t, lst, len(m))
}

func TestSimpleDao_SaveById(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV43{}))

	dao := NewSimpleDao[uint, modelV43](Config{DB: db})
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "just do it", SomeInt: 42}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hey yo", SomeInt: 106}).Error)

	ok, err := dao.SaveById(1, &modelV43{SomeString: "dont do it", SomeInt: 42})
	assert.NoError(t, err)
	assert.True(t, ok)
	m := &modelV43{}
	assert.NoError(t, db.Find(&m, 1).Error)
	assert.Equal(t, "dont do it", m.SomeString)
	assert.Equal(t, 42, m.SomeInt)
}

func TestSimpleDao_SaveById_K4L(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV43{}))

	dao := NewSimpleDao[uint, modelV43](Config{DB: db})
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "just do it", SomeInt: 42}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hey yo", SomeInt: 106}).Error)

	m := &modelV43{SomeString: "dont do it", SomeInt: 43}
	ok, err := dao.K4L().SaveById(1, m)
	assert.NoError(t, err)
	assert.True(t, ok)
	assert.NotEqual(t, 1, m.ID)
	assert.NoError(t, db.Find(&m, m.ID).Error)
	assert.Equal(t, "dont do it", m.SomeString)
	assert.Equal(t, 43, m.SomeInt)
	assert.Equal(t, 1, m.ParentID)
	p := &modelV43{}
	assert.NoError(t, db.Unscoped().Find(&p, 1).Error)
	assert.Equal(t, "just do it", p.SomeString)
	assert.Equal(t, 42, p.SomeInt)
	assert.Equal(t, int(m.ID), p.ChildID)
}

type badModelV43 struct {
	gorm.Model
	SomeString string
	SomeInt    int
	ChildID    int `unrelated:"childkey"`
	ParentID   int `unrelated:"parentkey"`
}

func TestSimpleDao_SaveById_K4L_BadModel(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&badModelV43{}))

	dao := NewSimpleDao[uint, badModelV43](Config{DB: db})
	assert.NoError(t, db.Create(&badModelV43{Model: gorm.Model{ID: 1}, SomeString: "just do it", SomeInt: 42}).Error)
	assert.NoError(t, db.Create(&badModelV43{Model: gorm.Model{ID: 3}, SomeString: "hey yo", SomeInt: 106}).Error)

	m := &badModelV43{SomeString: "dont do it", SomeInt: 43}
	ok, err := dao.K4L().SaveById(1, m)
	assert.NoError(t, err)
	assert.True(t, ok)
	assert.NotEqual(t, 1, m.ID)
	assert.NoError(t, db.Find(&m, m.ID).Error)
	assert.Equal(t, "dont do it", m.SomeString)
	assert.Equal(t, 43, m.SomeInt)
	assert.Equal(t, 0, m.ParentID)
	p := &badModelV43{}
	assert.NoError(t, db.Unscoped().Find(&p, 1).Error)
	assert.Equal(t, "just do it", p.SomeString)
	assert.Equal(t, 42, p.SomeInt)
	assert.Equal(t, 0, p.ChildID)
}

func TestSimpleDao_SaveById_K4L_v2(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV43{}))

	dao := NewSimpleDao[uint, modelV43](Config{DB: db})
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "quick brown fox", SomeInt: 42}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "olala", SomeInt: 309}).Error)

	m := &modelV43{SomeString: "matador", SomeInt: 666}
	ok, err := dao.K4L().SaveById(1, m)
	assert.NoError(t, err)
	assert.True(t, ok)
	assert.NotEqual(t, 1, m.ID)
	assert.NoError(t, db.Find(&m, m.ID).Error)
	assert.Equal(t, "matador", m.SomeString)
	assert.Equal(t, 666, m.SomeInt)
	assert.Equal(t, 1, m.ParentID)
	lastId := m.ID
	m = &modelV43{SomeString: "2nd update", SomeInt: 555}
	ok, err = dao.K4L().SaveById(lastId, m)
	assert.NoError(t, err)
	assert.True(t, ok)
	assert.NotEqual(t, lastId, m.ID)
	assert.NoError(t, db.Find(&m, m.ID).Error)
	assert.Equal(t, "2nd update", m.SomeString)
	assert.Equal(t, 555, m.SomeInt)
	assert.Equal(t, int(lastId), m.ParentID)
	p := &modelV43{}
	assert.NoError(t, db.Unscoped().Find(&p, 1).Error)
	assert.Equal(t, "quick brown fox", p.SomeString)
	assert.Equal(t, 42, p.SomeInt)
	assert.Equal(t, int(lastId), p.ChildID)
}

func TestSimpleDao_FindById(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	testRecords := []modelV42{
		{Model: gorm.Model{ID: 1}, SomeString: "some nice string", SomeInt: 89},
		{Model: gorm.Model{ID: 2}, SomeString: "try to find me", SomeInt: 99},
		{Model: gorm.Model{ID: 3}, SomeString: "another one", SomeInt: 19},
		{Model: gorm.Model{ID: 7}, SomeString: "dumdum", SomeInt: 32},
	}
	for _, record := range testRecords {
		assert.NoError(t, db.Create(&record).Error)
	}
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	m1, err := dao.FindById(7)
	assert.NoError(t, err)
	assert.Equal(t, m1.ID, testRecords[3].ID)
	assert.Equal(t, m1.SomeInt, testRecords[3].SomeInt)
	m1, err = dao.FindById(2)
	assert.NoError(t, err)
	assert.Equal(t, m1.ID, testRecords[1].ID)
	assert.Equal(t, m1.SomeString, testRecords[1].SomeString)
}

func TestSimpleDao_FindAll(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	testRecords := []modelV42{
		{Model: gorm.Model{ID: 1}, SomeString: "some nice string", SomeInt: 89},
		{Model: gorm.Model{ID: 2}, SomeString: "try to find me", SomeInt: 99},
		{Model: gorm.Model{ID: 3}, SomeString: "another one", SomeInt: 19},
		{Model: gorm.Model{ID: 7}, SomeString: "dumdum", SomeInt: 32},
	}
	for _, record := range testRecords {
		assert.NoError(t, db.Create(&record).Error)
	}
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	m, err := dao.FindAll()
	assert.NoError(t, err)
	assert.Len(t, m, len(testRecords))
	for i, r := range testRecords {
		assert.Equal(t, m[i].ID, r.ID)
		assert.Equal(t, m[i].SomeInt, r.SomeInt)
		assert.Equal(t, m[i].SomeString, r.SomeString)
	}
}

func TestSimpleDao_DeleteById(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	testRecords := []modelV42{
		{Model: gorm.Model{ID: 1}, SomeString: "some nice string", SomeInt: 89},
		{Model: gorm.Model{ID: 2}, SomeString: "try to find me", SomeInt: 99},
		{Model: gorm.Model{ID: 3}, SomeString: "another one", SomeInt: 19},
		{Model: gorm.Model{ID: 7}, SomeString: "dumdum", SomeInt: 32},
	}
	for _, record := range testRecords {
		assert.NoError(t, db.Create(&record).Error)
	}
	deleteId := uint(7)
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	ok, err := dao.DeleteById(deleteId)
	assert.NoError(t, err)
	assert.True(t, ok)
	m := &modelV42{}
	assert.NoError(t, db.Find(m, deleteId).Error)
	assert.Empty(t, m)
	assert.NoError(t, db.Unscoped().Find(m, deleteId).Error)
	assert.NotEmpty(t, m)
	c := int64(0)
	assert.NoError(t, db.Model(&modelV42{}).Count(&c).Error)
	assert.Equal(t, int64(3), c)
	assert.NoError(t, db.Model(&modelV42{}).Unscoped().Count(&c).Error)
	assert.Equal(t, int64(4), c)
}

func TestSimpleDao_RestoreById(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	testRecords := []modelV42{
		{Model: gorm.Model{ID: 1}, SomeString: "some nice string", SomeInt: 89},
		{Model: gorm.Model{ID: 2}, SomeString: "try to find me", SomeInt: 99},
		{Model: gorm.Model{ID: 3}, SomeString: "another one", SomeInt: 19},
		{Model: gorm.Model{ID: 7}, SomeString: "dumdum", SomeInt: 32},
	}
	for _, record := range testRecords {
		assert.NoError(t, db.Create(&record).Error)
	}
	deleteId := uint(2)
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	assert.NoError(t, db.Delete(&modelV42{}, deleteId).Error)
	m := &modelV42{}
	assert.NoError(t, db.Find(m, deleteId).Error)
	assert.Empty(t, m)
	assert.NoError(t, db.Unscoped().Find(m, deleteId).Error)
	assert.NotEmpty(t, m)
	ok, err := dao.RestoreById(42)
	assert.NoError(t, err)
	assert.False(t, ok)
	ok, err = dao.RestoreById(deleteId)
	assert.NoError(t, err)
	assert.True(t, ok)
}

func TestSimpleDao_DeleteByIdMe2(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	testRecords := []modelV42{
		{Model: gorm.Model{ID: 1}, SomeString: "some nice string", SomeInt: 89},
		{Model: gorm.Model{ID: 2}, SomeString: "try to find me", SomeInt: 99},
		{Model: gorm.Model{ID: 3}, SomeString: "another one", SomeInt: 19},
		{Model: gorm.Model{ID: 7}, SomeString: "dumdum", SomeInt: 32},
	}
	for _, record := range testRecords {
		assert.NoError(t, db.Create(&record).Error)
	}
	deleteId := uint(7)
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	ok, err := dao.Me2().DeleteById(deleteId)
	assert.NoError(t, err)
	assert.True(t, ok)
	m := &modelV42{}
	assert.NoError(t, db.Find(m, deleteId).Error)
	assert.Empty(t, m)
	assert.NoError(t, db.Unscoped().Find(m, deleteId).Error)
	assert.Empty(t, m)
	c := int64(0)
	assert.NoError(t, db.Model(&modelV42{}).Unscoped().Count(&c).Error)
	assert.Equal(t, int64(3), c)
}

func TestSimpleDao_PruneMe2(t *testing.T) {
	db := data.Connect("file:memdb1?mode=memory")
	assert.NoError(t, db.AutoMigrate(&modelV42{}))
	assert.NoError(t, db.Create(&modelV42{SomeString: "test"}).Error)
	assert.NoError(t, db.Create(&modelV42{SomeString: "something"}).Error)
	assert.NoError(t, db.Create(&modelV42{SomeString: "nothing"}).Error)
	assert.NoError(t, db.Create(&modelV42{SomeString: "dumdum"}).Error)
	var c int64
	db.Model(&modelV42{}).Count(&c)
	assert.Equal(t, int64(4), c)

	var m []modelV42
	dao := NewSimpleDao[uint, modelV42](Config{DB: db})
	assert.NoError(t, dao.Me2().Prune())
	assert.NoError(t, db.Unscoped().Find(&m).Error)
	assert.Len(t, m, 0)
}
