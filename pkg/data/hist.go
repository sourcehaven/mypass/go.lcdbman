package data

import (
	"errors"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/internal/fields"
	"gorm.io/gorm"
	"reflect"
)

var (
	NoTagError         = errors.New("does not have tag")
	DbUninitializedErr = errors.New("db is not yet initialized")
)

type hst[T any] struct {
	db *gorm.DB
}

type History[T any] interface {
	HistUp(m T) (r T, err error)
	HistDown(m T) (r T, err error)
	Ancestors(m T) (r []T, err error)
	Descendants(m T) (r []T, err error)
}

// NewHist creates a resource implementing the History interface.
func NewHist[T any](db *gorm.DB, _ T) History[T] {
	return &hst[T]{db: db}
}

// hist is the driver method for HistUp, and HistDown.
// This function basically implements all the functionalities.
func hist[T any](db *gorm.DB, m T, tag string) (r T, err error) {
	if db == nil {
		panic(DbUninitializedErr)
	}

	f, ok := fields.GetTag(m, "gormx", tag)
	if !ok {
		err = NoTagError
		return
	}
	field := reflect.ValueOf(m)
	if field.Kind() == reflect.Ptr {
		field = field.Elem()
	}
	field = field.FieldByName(f.Name)
	// We are working with gorm.Model, so we know that
	// the primary key, should be uint
	var id uint
	if field.CanConvert(reflect.TypeFor[uint]()) {
		id = uint(field.Convert(reflect.TypeFor[uint]()).Uint())
	}
	if err = db.Unscoped().Find(&r, id).Error; err != nil {
		return
	}
	return
}

// HistUp gets the parent of the passed record,
// if there is such an element.
func (h *hst[T]) HistUp(m T) (r T, err error) {
	return hist(h.db, m, "parentkey")
}

// HistDown gets the child of the passed record,
// if there is such an element.
func (h *hst[T]) HistDown(m T) (r T, err error) {
	return hist(h.db, m, "childkey")
}

// collectRelatives is the driver function for Ancestors, and Descendants.
// This function is responsible for collecting all the parents, or children
// of a requested record.
func collectRelatives[T any](db *gorm.DB, m T, tag string) (r []T, err error) {
	if db == nil {
		panic(DbUninitializedErr)
	}

	for {
		if m, err = hist(db, m, tag); err != nil {
			return
		}
		if reflect.ValueOf(m).Elem().FieldByName("ID").Uint() == 0 {
			return
		}
		r = append(r, m)
	}
}

// Ancestors collects all parents, that is all the whole ancestor tree.
func (h *hst[T]) Ancestors(m T) (r []T, err error) {
	return collectRelatives(h.db, m, "parentkey")
}

// Descendants collects all children, that is the whole descendant tree.
func (h *hst[T]) Descendants(m T) (r []T, err error) {
	return collectRelatives(h.db, m, "childkey")
}

// HistUp gets the parent of the passed record,
// if there is such an element.
func HistUp[T any](m T) (r T, err error) {
	return hist(defaultDB, m, "parentkey")
}

// HistDown gets the child of the passed record,
// if there is such an element.
func HistDown[T any](m T) (r T, err error) {
	return hist(defaultDB, m, "childkey")
}

// Ancestors collects all parents, that is all the whole ancestor tree.
func Ancestors[T any](m T) (r []T, err error) {
	return collectRelatives(defaultDB, m, "parentkey")
}

// Descendants collects all children, that is the whole descendant tree.
func Descendants[T any](m T) (r []T, err error) {
	return collectRelatives(defaultDB, m, "childkey")
}
