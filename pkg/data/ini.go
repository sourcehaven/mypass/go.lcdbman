package data

import "gorm.io/gorm"

var defaultDB *gorm.DB

// Init initializes the data package with default options.
// HistUp, HistDown, Ancestors, Descendants functions need
// this initialization to work correctly.
func Init(db *gorm.DB) {
	defaultDB = db
}
