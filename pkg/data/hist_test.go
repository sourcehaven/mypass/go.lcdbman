package data

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/internal/data"
	"gorm.io/gorm"
	"testing"
)

func TestHistUp(t *testing.T) {
	db := data.Connect("file::memory:")
	err := db.AutoMigrate(&modelV43{})
	Init(db)
	assert.NoError(t, err)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "asdasd", SomeInt: 58, ParentID: 0, ChildID: 0}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 2}, SomeString: "ayayaaa!!", SomeInt: 16, ParentID: 0, ChildID: 3}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hujujujj", SomeInt: 42, ParentID: 2}).Error)

	v, err := NewHist(db, &modelV43{}).HistUp(&modelV43{ParentID: 2})
	assert.NoError(t, err)
	assert.Equal(t, "ayayaaa!!", v.SomeString)
	assert.Equal(t, 16, v.SomeInt)
	assert.Equal(t, 0, v.ParentID)
	v, err = HistUp(&modelV43{ParentID: 2})
	assert.NoError(t, err)
	assert.Equal(t, "ayayaaa!!", v.SomeString)
	assert.Equal(t, 16, v.SomeInt)
	assert.Equal(t, 0, v.ParentID)
}

func TestHistDown(t *testing.T) {
	db := data.Connect("file::memory:")
	err := db.AutoMigrate(&modelV43{})
	Init(db)
	assert.NoError(t, err)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "asdasd", SomeInt: 58, ParentID: 0, ChildID: 0}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 2}, SomeString: "ayayaaa!!", SomeInt: 16, ParentID: 0, ChildID: 3}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hujujujj", SomeInt: 42, ParentID: 2}).Error)

	v, err := NewHist(db, &modelV43{}).HistDown(&modelV43{ChildID: 3})
	assert.NoError(t, err)
	assert.Equal(t, "hujujujj", v.SomeString)
	assert.Equal(t, 42, v.SomeInt)
	assert.Equal(t, uint(3), v.ID)
	v, err = HistDown(&modelV43{ChildID: 3})
	assert.NoError(t, err)
	assert.Equal(t, "hujujujj", v.SomeString)
	assert.Equal(t, 42, v.SomeInt)
	assert.Equal(t, uint(3), v.ID)
}

func TestAncestors(t *testing.T) {
	db := data.Connect("file::memory:")
	err := db.AutoMigrate(&modelV43{})
	assert.NoError(t, err)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "asdasd", SomeInt: 58, ParentID: 0, ChildID: 0}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 2}, SomeString: "ayayaaa!!", SomeInt: 16, ParentID: 1, ChildID: 3}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hujujujj", SomeInt: 42, ParentID: 2}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 4}, SomeString: "ejjejee!", SomeInt: 654, ParentID: 3}).Error)

	v, err := NewHist(db, &modelV43{}).Ancestors(&modelV43{ParentID: 3})
	assert.NoError(t, err)
	assert.Len(t, v, 3)
	v, err = NewHist(db, &modelV43{}).Ancestors(&modelV43{ParentID: 66})
	assert.NoError(t, err)
	assert.Len(t, v, 0)
}

func TestDescendants(t *testing.T) {
	db := data.Connect("file::memory:")
	err := db.AutoMigrate(&modelV43{})
	assert.NoError(t, err)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 1}, SomeString: "asdasd", SomeInt: 58, ParentID: 0, ChildID: 0}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 2}, SomeString: "ayayaaa!!", SomeInt: 16, ParentID: 1, ChildID: 3}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 3}, SomeString: "hujujujj", SomeInt: 42, ParentID: 2, ChildID: 4}).Error)
	assert.NoError(t, db.Create(&modelV43{Model: gorm.Model{ID: 4}, SomeString: "ejjejee!", SomeInt: 654, ParentID: 3}).Error)

	v, err := NewHist(db, &modelV43{}).Descendants(&modelV43{ChildID: 2})
	assert.NoError(t, err)
	assert.Len(t, v, 3)
	v, err = NewHist(db, &modelV43{}).Descendants(&modelV43{ParentID: 66})
	assert.NoError(t, err)
	assert.Len(t, v, 0)
}

func TestUninitialized(t *testing.T) {
	assert.Panics(t, func() { _, _ = NewHist(nil, &modelV43{}).Descendants(&modelV43{}) })
	assert.Panics(t, func() { _, _ = NewHist(nil, &modelV43{}).Ancestors(&modelV43{}) })
	assert.Panics(t, func() { _, _ = NewHist(nil, &modelV43{}).HistDown(&modelV43{}) })
	assert.Panics(t, func() { _, _ = NewHist(nil, &modelV43{}).HistUp(&modelV43{}) })
}
