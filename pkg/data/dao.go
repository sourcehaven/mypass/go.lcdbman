/**
The main purpose of this file is to implement some
abstractions over database access. Some objects require
more delicate handling when saved, updated, deleted,
or listed. These special handles are implemented here.
*/

package data

import (
	"errors"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/internal/data"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/internal/fields"
	"gorm.io/gorm"
	"reflect"
)

var (
	NotGormModel = errors.New("the model type provided is not a gorm model")
)

type T any

type PK interface {
	~int | ~int32 | ~int64 | ~uint | ~uint32 | ~uint64 | ~string
}

type Config struct {
	DB *gorm.DB
}

type simpleDao[Pk PK, Tp T] struct {
	DB         *gorm.DB
	Keep4Later bool
}

// Dao helps you with managing your persisted
// objects, database records. Every managed
// type should have a Model gorm.Model
// field as its embedded type. This is
// a mandatory requirement.
type Dao[Pk PK, Tp T] interface {
	// Tx creates a transactional dao interface.
	Tx() Dao[Pk, Tp]
	// Cmt commits the transaction,
	// as in persists the changes done so far.
	Cmt() Dao[Pk, Tp]
	// Me2 allows the query to include unscoped,
	// deleted records screaming "me too!".
	Me2() Dao[Pk, Tp]
	// K4L as in keep for later,
	// switches to a mode that allows
	// keeping track of historical changes,
	// possibly made by updates and such.
	K4L() Dao[Pk, Tp]
	// Create persists a new record.
	// Created model can not contain an existing
	// ID field.
	Create(m *Tp) (err error)
	// CreateAll persists all records. The models to
	// be saved can not contain existing ID fields.
	CreateAll(m []*Tp) (err error)
	// SaveById saves a record as-is,
	// overwriting all fields of the model
	// specified by its id. If K4L mode is applied,
	// then instead of overwriting the given record,
	// this method will duplicate and update the
	// duplicated copy, while soft deleting the old one.
	// All fields will be saved, even if they contain
	// default values.
	SaveById(id Pk, m *Tp) (ok bool, err error)
	// UpdateById updates a record,
	// only updating columns with non-default values.
	// If K4L mode is applied, then this method does
	// not update fields directly, but instead makes
	// a copy of the given record, and updates that
	// copy, then soft deletes the old one.
	// Only non-null fields will be updated.
	UpdateById(id Pk, m *Tp) (ok bool, err error)
	// FindById fetches a single value
	// from the db by a given id.
	// If Me2 mode is applied, then even soft deleted
	// records will be contained in the query.
	FindById(id Pk) (m Tp, err error)
	// FindAll fetches all records from a
	// specific table, given by the model interface.
	// If Me2 mode is applied, then even soft deleted
	// records will be contained in the query.
	FindAll() (m []Tp, err error)
	// DeleteById deletes a given record by its id.
	// If Me2 mode is applied, records will be
	// permanently deleted.
	DeleteById(id Pk) (ok bool, err error)
	// RestoreById function is responsible for restoring
	// a soft deleted record, if there is any.
	RestoreById(id Pk) (ok bool, err error)
	// Prune deletes all records within matching table.
	// If Me2 mode is applied, then the records will be
	// permanently deleted.
	Prune() (err error)
	// Count returns the row count of the given table.
	// If Me2 mode is applied, then even soft deleted
	// records will be counted.
	Count() (n int64, err error)
}

func panicNotGormModel(m any) {
	t := reflect.TypeOf(m)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	f, ok := t.FieldByName("Model")
	if !ok || f.Type != reflect.TypeOf(gorm.Model{}) {
		panic(NotGormModel)
	}
}

func NewSimpleDao[Pk PK, Tp T](c ...Config) Dao[Pk, Tp] {
	panicNotGormModel(new(Tp))
	if len(c) == 0 {
		return &simpleDao[Pk, Tp]{}
	}
	cfg := c[0]
	return &simpleDao[Pk, Tp]{DB: cfg.DB}
}

func (d *simpleDao[Pk, Tp]) Tx() Dao[Pk, Tp] {
	return &simpleDao[Pk, Tp]{DB: d.DB.Begin(), Keep4Later: d.Keep4Later}
}

func (d *simpleDao[Pk, Tp]) Cmt() Dao[Pk, Tp] {
	return &simpleDao[Pk, Tp]{DB: d.DB.Commit(), Keep4Later: d.Keep4Later}
}

func (d *simpleDao[Pk, Tp]) Me2() Dao[Pk, Tp] {
	return &simpleDao[Pk, Tp]{DB: d.DB.Unscoped(), Keep4Later: d.Keep4Later}
}

func (d *simpleDao[Pk, Tp]) K4L() Dao[Pk, Tp] {
	return &simpleDao[Pk, Tp]{DB: d.DB, Keep4Later: true}
}

func (d *simpleDao[Pk, Tp]) Create(m *Tp) (err error) {
	return d.DB.Create(m).Error
}

func (d *simpleDao[Pk, Tp]) CreateAll(m []*Tp) (err error) {
	return d.DB.Transaction(func(tx *gorm.DB) (err error) {
		for i := range m {
			if err = tx.Create(m[i]).Error; err != nil {
				return
			}
		}
		return
	})
}

func (d *simpleDao[Pk, Tp]) SaveById(id Pk, m *Tp) (ok bool, err error) {
	idVal := reflect.ValueOf(id)
	idU64 := idVal.Uint()

	// region Simple mode
	if !d.Keep4Later {
		reflect.ValueOf(m).Elem().FieldByName("ID").Set(idVal)
		tx := d.DB.Save(m)
		return tx.RowsAffected != 0, tx.Error
	}
	// endregion Simple mode

	// region Keep for later mode
	rowsAffected := int64(0)
	err = d.DB.Transaction(func(tx *gorm.DB) (err error) {
		// Fetch the object with its current id
		curr := new(Tp)
		if err = tx.First(curr, id).Error; err != nil {
			return
		}
		// Create the same model, but strip gorm attribs
		reflect.ValueOf(curr).Elem().FieldByName("Model").Set(reflect.ValueOf(gorm.Model{}))
		var op *gorm.DB
		if op = tx.Create(curr); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected

		// Update old model's child attribute (save contains pk)
		oQuery := &gorm.Model{ID: uint(idU64)}
		child := new(Tp)
		currId := reflect.ValueOf(curr).Elem().FieldByName("ID")
		f, ok := fields.GetTag(child, "gormx", "childkey")
		if ok {
			field := reflect.ValueOf(child).Elem().FieldByName(f.Name)
			field.Set(reflect.ValueOf(data.Convert(currId.Uint(), field.Type())))
			if op = tx.Where(oQuery).Updates(child); op.Error != nil {
				err = op.Error
				return
			}
			rowsAffected += op.RowsAffected
		}
		// Soft delete old model
		empty := new(Tp)
		if op = tx.Delete(empty, id); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected

		// Update new model's parent attribute
		reflect.ValueOf(m).Elem().FieldByName("ID").Set(currId)
		f, ok = fields.GetTag(child, "gormx", "parentkey")
		if ok {
			field := reflect.ValueOf(m).Elem().FieldByName(f.Name)
			field.Set(reflect.ValueOf(data.Convert(idU64, field.Type())))
		}

		// Update currently created model instead of the original
		if op = tx.Save(m); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected
		return
	})
	ok = rowsAffected != 0
	return
	// endregion Keep for later mode
}

func (d *simpleDao[Pk, Tp]) UpdateById(id Pk, m *Tp) (ok bool, err error) {
	idVal := reflect.ValueOf(id)
	idU64 := idVal.Uint()

	// region Simple mode
	if !d.Keep4Later {
		tx := d.DB.Where(&struct{ ID Pk }{ID: id}).Updates(m)
		return tx.RowsAffected != 0, tx.Error
	}
	// endregion Simple mode

	// region Keep for later mode
	rowsAffected := int64(0)
	err = d.DB.Transaction(func(tx *gorm.DB) (err error) {
		// Fetch the object with its current id
		curr := new(Tp)
		if err = tx.First(curr, id).Error; err != nil {
			return
		}
		// Create the same model, but strip gorm attribs
		reflect.ValueOf(curr).Elem().FieldByName("Model").Set(reflect.ValueOf(gorm.Model{}))
		var op *gorm.DB
		if op = tx.Create(curr); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected
		// Update old model's child attribute (save contains pk)
		oQuery := &gorm.Model{ID: uint(idU64)}
		child := new(Tp)
		currId := reflect.ValueOf(curr).Elem().FieldByName("ID")
		f, ok := fields.GetTag(child, "gormx", "childkey")
		if ok {
			field := reflect.ValueOf(child).Elem().FieldByName(f.Name)
			field.Set(reflect.ValueOf(data.Convert(currId.Uint(), field.Type())))
			if op = tx.Where(oQuery).Updates(child); op.Error != nil {
				err = op.Error
				return
			}
			rowsAffected += op.RowsAffected
		}
		// Soft delete old model
		empty := new(Tp)
		if op = tx.Delete(empty, id); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected
		cQuery := &gorm.Model{ID: uint(currId.Uint())}

		// Update new model's parent attribute
		reflect.ValueOf(m).Elem().FieldByName("ID").Set(reflect.ValueOf(cQuery.ID))
		f, ok = fields.GetTag(child, "gormx", "parentkey")
		if ok {
			field := reflect.ValueOf(m).Elem().FieldByName(f.Name)
			field.Set(reflect.ValueOf(data.Convert(idU64, field.Type())))
		}

		// Update currently created model instead of the original
		if op = tx.Where(cQuery).Updates(m); op.Error != nil {
			err = op.Error
			return
		}
		rowsAffected += op.RowsAffected
		return
	})
	ok = rowsAffected != 0
	return
	// endregion Keep for later mode
}

func (d *simpleDao[Pk, Tp]) FindById(id Pk) (m Tp, err error) {
	return m, d.DB.First(&m, id).Error
}

func (d *simpleDao[Pk, Tp]) FindAll() (m []Tp, err error) {
	return m, d.DB.Find(&m).Error
}

func (d *simpleDao[Pk, Tp]) DeleteById(id Pk) (ok bool, err error) {
	tx := d.DB.Delete(new(Tp), id)
	return tx.RowsAffected != 0, tx.Error
}

func (d *simpleDao[Pk, Tp]) RestoreById(id Pk) (ok bool, err error) {
	tx := d.DB.Unscoped().Model(new(Tp)).Where(&struct{ ID Pk }{ID: id}).Update("deleted_at", nil)
	return tx.RowsAffected != 0, tx.Error
}

func (d *simpleDao[Pk, Tp]) Prune() (err error) {
	return d.DB.Session(&gorm.Session{AllowGlobalUpdate: true}).Delete(new(Tp)).Error
}

func (d *simpleDao[Pk, Tp]) Count() (n int64, err error) {
	err = d.DB.Model(new(Tp)).Count(&n).Error
	return
}
