package data

import (
	"database/sql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func Connect(conn string, c ...*gorm.Config) (db *gorm.DB) {
	// Prepare default config if empty
	conf := &gorm.Config{}
	if len(c) > 0 {
		conf = c[0]
	}

	// Connect with specific driver
	dbConn, err := sql.Open("sqlite3", conn)
	if err != nil {
		panic("failed to open database connection")
	}
	// Try creating database connection with specific dialect
	if db, err = gorm.Open(sqlite.Dialector{Conn: dbConn}, conf); err != nil {
		panic("failed to open database connection")
	}
	return
}
