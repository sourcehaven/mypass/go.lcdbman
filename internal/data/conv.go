package data

import (
	"fmt"
	"reflect"
)

// Convert is a utility function
// to convert between basic types.
func Convert(v any, t reflect.Type) any {
	vt := reflect.TypeOf(v)
	if vt.Name() == t.Name() {
		return v
	}

	switch t.Name() {
	case "int":
		switch v.(type) {
		case int8:
			return int(v.(int8))
		case int16:
			return int(v.(int16))
		case int32:
			return int(v.(int32))
		case int64:
			return int(v.(int64))
		case uint:
			return int(v.(uint))
		case uint8:
			return int(v.(uint8))
		case uint16:
			return int(v.(uint16))
		case uint32:
			return int(v.(uint32))
		case uint64:
			return int(v.(uint64))
		case float32:
			return int(v.(float32))
		case float64:
			return int(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "int8":
		switch v.(type) {
		case int:
			return int8(v.(int))
		case int16:
			return int8(v.(int16))
		case int32:
			return int8(v.(int32))
		case int64:
			return int8(v.(int64))
		case uint:
			return int8(v.(uint))
		case uint8:
			return int8(v.(uint8))
		case uint16:
			return int8(v.(uint16))
		case uint32:
			return int8(v.(uint32))
		case uint64:
			return int8(v.(uint64))
		case float32:
			return int8(v.(float32))
		case float64:
			return int8(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "int16":
		switch v.(type) {
		case int:
			return int16(v.(int))
		case int8:
			return int16(v.(int8))
		case int32:
			return int16(v.(int32))
		case int64:
			return int16(v.(int64))
		case uint:
			return int16(v.(uint))
		case uint8:
			return int16(v.(uint8))
		case uint16:
			return int16(v.(uint16))
		case uint32:
			return int16(v.(uint32))
		case uint64:
			return int16(v.(uint64))
		case float32:
			return int16(v.(float32))
		case float64:
			return int16(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "int32":
		switch v.(type) {
		case int:
			return int32(v.(int))
		case int8:
			return int32(v.(int8))
		case int16:
			return int32(v.(int16))
		case int64:
			return int32(v.(int64))
		case uint:
			return int32(v.(uint))
		case uint8:
			return int32(v.(uint8))
		case uint16:
			return int32(v.(uint16))
		case uint32:
			return int32(v.(uint32))
		case uint64:
			return int32(v.(uint64))
		case float32:
			return int32(v.(float32))
		case float64:
			return int32(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "int64":
		switch v.(type) {
		case int:
			return int64(v.(int))
		case int8:
			return int64(v.(int8))
		case int16:
			return int64(v.(int16))
		case int32:
			return int64(v.(int32))
		case uint:
			return int64(v.(uint))
		case uint8:
			return int64(v.(uint8))
		case uint16:
			return int64(v.(uint16))
		case uint32:
			return int64(v.(uint32))
		case uint64:
			return int64(v.(uint64))
		case float32:
			return int64(v.(float32))
		case float64:
			return int64(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "uint":
		switch v.(type) {
		case int:
			return uint(v.(int))
		case int8:
			return uint(v.(int8))
		case int16:
			return uint(v.(int16))
		case int32:
			return uint(v.(int32))
		case int64:
			return uint(v.(int64))
		case uint8:
			return uint(v.(uint8))
		case uint16:
			return uint(v.(uint16))
		case uint32:
			return uint(v.(uint32))
		case uint64:
			return uint(v.(uint64))
		case float32:
			return uint(v.(float32))
		case float64:
			return uint(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "uint8":
		switch v.(type) {
		case int:
			return uint8(v.(int))
		case int8:
			return uint8(v.(int8))
		case int16:
			return uint8(v.(int16))
		case int32:
			return uint8(v.(int32))
		case int64:
			return uint8(v.(int64))
		case uint:
			return uint8(v.(uint))
		case uint16:
			return uint8(v.(uint16))
		case uint32:
			return uint8(v.(uint32))
		case uint64:
			return uint8(v.(uint64))
		case float32:
			return uint8(v.(float32))
		case float64:
			return uint8(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "uint16":
		switch v.(type) {
		case int:
			return uint16(v.(int))
		case int8:
			return uint16(v.(int8))
		case int16:
			return uint16(v.(int16))
		case int32:
			return uint16(v.(int32))
		case int64:
			return uint16(v.(int64))
		case uint:
			return uint16(v.(uint))
		case uint8:
			return uint16(v.(uint8))
		case uint32:
			return uint16(v.(uint32))
		case uint64:
			return uint16(v.(uint64))
		case float32:
			return uint16(v.(float32))
		case float64:
			return uint16(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "uint32":
		switch v.(type) {
		case int:
			return uint32(v.(int))
		case int8:
			return uint32(v.(int8))
		case int16:
			return uint32(v.(int16))
		case int32:
			return uint32(v.(int32))
		case int64:
			return uint32(v.(int64))
		case uint:
			return uint32(v.(uint))
		case uint8:
			return uint32(v.(uint8))
		case uint16:
			return uint32(v.(uint16))
		case uint64:
			return uint32(v.(uint64))
		case float32:
			return uint32(v.(float32))
		case float64:
			return uint32(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	case "uint64":
		switch v.(type) {
		case int:
			return uint64(v.(int))
		case int8:
			return uint64(v.(int8))
		case int16:
			return uint64(v.(int16))
		case int32:
			return uint64(v.(int32))
		case int64:
			return uint64(v.(int64))
		case uint:
			return uint64(v.(uint))
		case uint8:
			return uint64(v.(uint8))
		case uint16:
			return uint64(v.(uint16))
		case uint32:
			return uint64(v.(uint32))
		case float32:
			return uint64(v.(float32))
		case float64:
			return uint64(v.(float64))
		default:
			panic(fmt.Sprintf("unknown type %T", v))
		}
	}
	panic(fmt.Sprintf("cannot convert %v to %s", v, t.Name()))
}
