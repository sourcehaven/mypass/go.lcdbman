package fields

import (
	"reflect"
	"strings"
)

func GetTag(m any, tag string, cond ...string) (field reflect.StructField, ok bool) {
	f := reflect.TypeOf(m)
	if f.Kind() == reflect.Ptr {
		f = f.Elem()
	}
	for i := 0; i < f.NumField(); i++ {
		t := reflect.TypeOf(m)
		if t.Kind() == reflect.Ptr {
			t = t.Elem()
		}
		f := t.Field(i)
		v, ok := f.Tag.Lookup(tag)
		if !ok {
			continue
		}
		all := true
		for _, cond := range cond {
			all = all && strings.Contains(v, cond)
		}
		if all {
			return f, true
		}
	}
	return reflect.StructField{}, false
}
