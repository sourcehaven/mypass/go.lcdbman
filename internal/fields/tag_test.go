package fields

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

type dummyGormMP struct {
	ID         uint `gorm:"primarykey;someotherkey"`
	SomeString string
	ChiID      uint `gormx:"childkey"`
	ParID      uint `gormx:"parentkey"`
}

func Test_getGormTag_Child(t *testing.T) {
	o := &dummyGormMP{SomeString: "some string", ChiID: 5, ParID: 1}
	f, ok := GetTag(o, "gormx", "childkey")
	assert.True(t, ok)
	chField, ok := reflect.TypeOf(o).Elem().FieldByName("ChiID")
	assert.True(t, ok)
	assert.Equal(t, f, chField)
}

func Test_getGormTag_Parent(t *testing.T) {
	o := &dummyGormMP{SomeString: "some string", ChiID: 5, ParID: 1}
	f, ok := GetTag(o, "gormx", "parentkey")
	assert.True(t, ok)
	parField, ok := reflect.TypeOf(o).Elem().FieldByName("ParID")
	assert.True(t, ok)
	assert.Equal(t, f, parField)
}

func Test_getGormTag_Multi(t *testing.T) {
	o := &dummyGormMP{SomeString: "some string", ChiID: 5, ParID: 1, ID: 10}
	f, ok := GetTag(o, "gorm", "primarykey", "someotherkey")
	assert.True(t, ok)
	parField, ok := reflect.TypeOf(o).Elem().FieldByName("ID")
	assert.True(t, ok)
	assert.Equal(t, f, parField)
}

func Test_getGormTag_InvalidMulti(t *testing.T) {
	o := &dummyGormMP{SomeString: "some string", ChiID: 5, ParID: 1, ID: 10}
	f, ok := GetTag(o, "gorm", "primarykey", "invalidkey")
	assert.False(t, ok)
	assert.Equal(t, f, reflect.StructField{})
}
