/**
PACKAGE RIP IS THE REST IN PIECE PACKAGE.
YOU SHOULD NOT BE USING IT IN ANY WAY.
*/

package rip

import "reflect"

type CommonStore interface {
	Save(d []any) error
	Fetch() ([]any, error)
}

type seri struct {
	store            CommonStore
	type2proto       map[reflect.Type]reflect.Type
	proto2type       map[reflect.Type]reflect.Type
	type2protoFields map[reflect.Type]map[string]string
	proto2typeFields map[reflect.Type]map[string]string
}

type SeriConfig struct {
	Store            CommonStore
	TypeMapping      map[reflect.Type]reflect.Type
	TypeFieldMapping map[reflect.Type]map[string]string
}

type ProtoSerializer interface {
	Dump() ([]byte, error)
	Load([]byte) error
}

func NewProtoSerializer(c SeriConfig) ProtoSerializer {
	InvTypeMapping := make(map[reflect.Type]reflect.Type)
	InvTypeFieldMapping := make(map[reflect.Type]map[string]string)
	for k, v := range c.TypeMapping {
		InvTypeMapping[v] = k
	}
	for k, v := range c.TypeFieldMapping {
		InvFieldMapping := make(map[string]string)
		for k, v := range v {
			InvFieldMapping[v] = k
		}
		InvTypeFieldMapping[c.TypeMapping[k]] = InvFieldMapping
	}
	return &seri{
		store:            c.Store,
		type2proto:       c.TypeMapping,
		proto2type:       InvTypeMapping,
		type2protoFields: c.TypeFieldMapping,
		proto2typeFields: InvTypeFieldMapping,
	}
}

func (s *seri) Dump() ([]byte, error) {
	//TODO implement me
	panic("implement me")
}

func (s *seri) Load(bytes []byte) error {
	//TODO implement me
	panic("implement me")
}
